Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  us/call  us/call  name    
 28.04      0.35     0.35  6669222     0.05     0.05  numberOfEmptyPositions
 14.42      0.53     0.18  7314856     0.02     0.02  checkColumns
  9.62      0.65     0.12  6986490     0.02     0.02  checkRows
  9.62      0.77     0.12  3291324     0.04     0.04  getEmptyPositions
  8.01      0.87     0.10  7643917     0.01     0.01  checkRightLeftDiagonal
  8.01      0.97     0.10   700000     0.14     1.66  simulate
  7.21      1.06     0.09  7911407     0.01     0.01  checkLeftRightDiagonal
  5.61      1.13     0.07   700000     0.10     0.10  selection
  3.61      1.18     0.05   700001     0.06     0.06  initBoard
  3.21      1.22     0.04  3991326     0.01     0.18  boardStatus
  1.60      1.24     0.02                             main
  1.20      1.25     0.02  3291324     0.00     0.00  switchPlayer
  0.00      1.25     0.00   700000     0.00     0.00  backPropogate
  0.00      1.25     0.00        7     0.00     0.00  destroyNode
  0.00      1.25     0.00        7     0.00     0.04  expandNode
  0.00      1.25     0.00        7     0.00     0.00  getMaxChildNode
  0.00      1.25     0.00        7     0.00     0.00  playNode
  0.00      1.25     0.00        1     0.00     0.18  printBoard

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.

 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012-2016 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 0.80% of 1.25 seconds

index % time    self  children    called     name
                                                 <spontaneous>
[1]    100.0    0.02    1.23                 main [1]
                0.10    1.06  700000/700000      simulate [2]
                0.07    0.00  700000/700000      selection [10]
                0.00    0.00       8/3991326     boardStatus [3]
                0.00    0.00       7/6669222     numberOfEmptyPositions [4]
                0.00    0.00       7/7           expandNode [13]
                0.00    0.00       1/1           printBoard [14]
                0.00    0.00       1/700001      initBoard [11]
                0.00    0.00       7/3291324     switchPlayer [12]
                0.00    0.00  700000/700000      backPropogate [15]
                0.00    0.00       7/7           getMaxChildNode [17]
                0.00    0.00       7/7           playNode [18]
                0.00    0.00       7/7           destroyNode [16]
-----------------------------------------------
                0.10    1.06  700000/700000      main [1]
[2]     92.8    0.10    1.06  700000         simulate [2]
                0.04    0.67 3991317/3991326     boardStatus [3]
                0.17    0.00 3291317/6669222     numberOfEmptyPositions [4]
                0.12    0.00 3291317/3291324     getEmptyPositions [7]
                0.05    0.00  700000/700001      initBoard [11]
                0.02    0.00 3291317/3291324     switchPlayer [12]
-----------------------------------------------
                0.00    0.00       1/3991326     printBoard [14]
                0.00    0.00       8/3991326     main [1]
                0.04    0.67 3991317/3991326     simulate [2]
[3]     56.6    0.04    0.67 3991326         boardStatus [3]
                0.18    0.00 7314856/7314856     checkColumns [5]
                0.18    0.00 3377898/6669222     numberOfEmptyPositions [4]
                0.12    0.00 6986490/6986490     checkRows [6]
                0.10    0.00 7643917/7643917     checkRightLeftDiagonal [8]
                0.09    0.00 7911407/7911407     checkLeftRightDiagonal [9]
-----------------------------------------------
                0.00    0.00       7/6669222     main [1]
                0.17    0.00 3291317/6669222     simulate [2]
                0.18    0.00 3377898/6669222     boardStatus [3]
[4]     28.0    0.35    0.00 6669222         numberOfEmptyPositions [4]
-----------------------------------------------
                0.18    0.00 7314856/7314856     boardStatus [3]
[5]     14.4    0.18    0.00 7314856         checkColumns [5]
-----------------------------------------------
                0.12    0.00 6986490/6986490     boardStatus [3]
[6]      9.6    0.12    0.00 6986490         checkRows [6]
-----------------------------------------------
                0.00    0.00       7/3291324     expandNode [13]
                0.12    0.00 3291317/3291324     simulate [2]
[7]      9.6    0.12    0.00 3291324         getEmptyPositions [7]
-----------------------------------------------
                0.10    0.00 7643917/7643917     boardStatus [3]
[8]      8.0    0.10    0.00 7643917         checkRightLeftDiagonal [8]
-----------------------------------------------
                0.09    0.00 7911407/7911407     boardStatus [3]
[9]      7.2    0.09    0.00 7911407         checkLeftRightDiagonal [9]
-----------------------------------------------
                0.07    0.00  700000/700000      main [1]
[10]     5.6    0.07    0.00  700000         selection [10]
-----------------------------------------------
                0.00    0.00       1/700001      main [1]
                0.05    0.00  700000/700001      simulate [2]
[11]     3.6    0.05    0.00  700001         initBoard [11]
-----------------------------------------------
                0.00    0.00       7/3291324     main [1]
                0.02    0.00 3291317/3291324     simulate [2]
[12]     1.2    0.02    0.00 3291324         switchPlayer [12]
-----------------------------------------------
                0.00    0.00       7/7           main [1]
[13]     0.0    0.00    0.00       7         expandNode [13]
                0.00    0.00       7/3291324     getEmptyPositions [7]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[14]     0.0    0.00    0.00       1         printBoard [14]
                0.00    0.00       1/3991326     boardStatus [3]
-----------------------------------------------
                0.00    0.00  700000/700000      main [1]
[15]     0.0    0.00    0.00  700000         backPropogate [15]
-----------------------------------------------
                0.00    0.00       7/7           main [1]
[16]     0.0    0.00    0.00       7         destroyNode [16]
-----------------------------------------------
                0.00    0.00       7/7           main [1]
[17]     0.0    0.00    0.00       7         getMaxChildNode [17]
-----------------------------------------------
                0.00    0.00       7/7           main [1]
[18]     0.0    0.00    0.00       7         playNode [18]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012-2016 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

  [15] backPropogate          [16] destroyNode             [4] numberOfEmptyPositions
   [3] boardStatus            [13] expandNode             [18] playNode
   [5] checkColumns            [7] getEmptyPositions      [14] printBoard
   [9] checkLeftRightDiagonal [17] getMaxChildNode        [10] selection
   [8] checkRightLeftDiagonal [11] initBoard               [2] simulate
   [6] checkRows               [1] main                   [12] switchPlayer
