#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define PLAYER_O 1
#define PLAYER_X 2

//Represents the state of the board
typedef enum Statusses
{
   IN_PROGRESS,
   PLAYER_O_WIN,
   PLAYER_X_WIN,
   DRAW
} Status;

//Struct containing the actual board and the current state of the board
typedef struct Boards 
{
   // array representing the board
   char** board_game;
   // size of the board
   int size;
   // player on the go
   char player;
} Board;

Board* initBoard(int size);
void switchPlayer(Board* board);
int* getEmptyPositions(Board board, int numberOfEmptyPositions);
int numberOfEmptyPositions(Board board);
Status boardStatus(Board board);
bool checkColumns(Board board, char player);
bool checkRows(Board board, char player);
bool checkRightLeftDiagnoal(Board board, char player);
bool checkLeftDiagonal(Board board, char player);

#endif
