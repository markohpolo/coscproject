#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "game.h"
Board* initBoard(int size) {
   Board* board = (Board*) malloc(sizeof(Board));
   board->board_game = (char**) calloc(size, sizeof(char*));
   for (int i = 0; i < size; i++)
      board->board_game[i] = (char*) calloc(size, sizeof(char));
   board->player = PLAYER_O;
   board->size = size;
   return board;
}

void switchPlayer(Board* board) {
   if (board->player == PLAYER_O) { board->player = PLAYER_X; }
   else { board->player = PLAYER_O; }
   return;
}

int* getEmptyPositions(Board board, int numberOfEmptyPositions) {
   // allocate memory for empty positions
   int* emptyPositions = (int*) calloc(numberOfEmptyPositions * 2, sizeof(int));
   // counter for emtpy spaces
   int counter = 0;
   for (int i = 0; i < board.size; i++) {
      for (int j = 0; j < board.size; j++) {
         // if empty position
         if (board.board_game[i][j] == 0) {
            // position of X
            emptyPositions[counter] = i;
            // position of Y
            emptyPositions[counter + 1] = j;
            counter += 2;
         }
      }
   }
   return emptyPositions;
}


int numberOfEmptyPositions(Board board) {
   int number_of_empty = 0;
   for (int i = 0; i < board.size; i++) {
      for (int j = 0; j < board.size; j++) {
         if (board.board_game[i][j] == 0) { number_of_empty++; }
      }
   }
   return number_of_empty;
}

bool checkColumns(Board board, char player) {
   for (int i = 0; i < board.size; i++) {
      for (int j = 0; j < board.size; j++) {
         if (board.board_game[j][i] != player) {
            break;
         }
         if (j == board.size - 1) {
            return true;
         }
      }
   }
   return false;
}

bool checkRows(Board board, char player) {
   for (int i = 0; i < board.size; i++) {
      for (int j = 0; j < board.size; j++) {
         if (board.board_game[i][j] != player) {
            break;
         }
         if (j == board.size - 1) {
            return true;
         }
      }
   }
   return false;
}

bool checkRightLeftDiagonal(Board board, char player) {
   for (int i = board.size - 1; i >= 0; i--) {
      if (board.board_game[board.size - 1 - i][i] != player) {
         return false;
      }
   }
   return true;
}

bool checkLeftRightDiagonal(Board board, char player) {
   for (int i = 0; i < board.size; i++) {
      if (board.board_game[i][i] != player) {
         return false;
      }
   }
   //if diagonal win
   return true;
}

Status boardStatus(Board board) {
   //check diagonal win for PLAYER_O
   if (checkLeftRightDiagonal(board, PLAYER_O)) { return PLAYER_O_WIN; }
   //check diagonal win for PLAYER_X
   if (checkLeftRightDiagonal(board, PLAYER_X)) { return PLAYER_X_WIN; }
   
   //check right diagonal win for PLAYER_O
   if (checkRightLeftDiagonal(board, PLAYER_O)) { return PLAYER_O_WIN; }
   //check right diagnoal win for PLAYER_X
   if (checkRightLeftDiagonal(board, PLAYER_X)) { return PLAYER_X_WIN; }
   
   //check column win for PLAYER_O
   if (checkColumns(board, PLAYER_O)) { return PLAYER_O_WIN; }
   //check column win for PLAYER_X
   if (checkColumns(board, PLAYER_X)) { return PLAYER_X_WIN; }
   
   //check row win for PLAYER_O
   if (checkRows(board, PLAYER_O)) { return PLAYER_O_WIN; }
   //check row win for PLAYER_X
   if (checkRows(board, PLAYER_X)) { return PLAYER_X_WIN; }
   
   //check if empty position existsif
   if (numberOfEmptyPositions(board) != 0) { return IN_PROGRESS; }
   //if none of the cases above, game is a draw
   return DRAW;

}
