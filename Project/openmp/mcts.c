#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <time.h>
#include <stdbool.h>
#include <limits.h>
#include <math.h>
#include <omp.h>
#include "game.h"
#include "node.h"

#define WIN_SCORE 10

void lessSmartPlayer(Board* board);
void printBoard(Board board);
void destroyNode(Node* root, int numberOfChildren);
void playNode(Board* board, Node* node);
Node* getMaxChildNode(Node* parent, int numberOfChild); 
Node* selection(Node* parentNode, int emptySpace);
void expandNode(Node* parentNode, Board board, int numberOfEmptyPositions);
Status simulate(Node* exploringNode, Board* board);
void backPropogate(Node* exploredNode, Board* board, Status simulationResult);

int main(int argc, char** argv) {
   Board* board = initBoard(atoi(argv[1]));
   Node* root;
   int emptyPositions;
   clock_t t, tWorld; //this will be the timer
   double expandTime = 0, selectionTime = 0, simulateTime = 0, backPropogateTime = 0;// 
   int whileLoopCount = 0;
   tWorld = clock();
   puts("Running...");
   //play while game is in progress
   //while (boardStatus(*board) == IN_PROGRESS) {
      whileLoopCount += 1;
      // set up the root node
      root = malloc(sizeof(Node));
      root->parent = NULL;
      root->children = NULL;
      t = clock(); // timer for node expansion
      // get the number of empty positions
      emptyPositions = numberOfEmptyPositions(*board);
      // expand node once per simulation
      expandNode(root, *board, emptyPositions);
      t = clock() - t;
      expandTime += ((double)t)/(CLOCKS_PER_SEC);
      // number of simulations to be ran: 100000
      for (int i = 0; i < 100000; i++) {
         //select a node
         t = clock();
         Node* selectedNode = selection(root, emptyPositions);
         t = clock() - t;
         selectionTime += ((double)t)/(CLOCKS_PER_SEC/1000);
         
         //run simulation
         t = clock();
         Status status = simulate(selectedNode, board);
         t = clock() - t;
         simulateTime += ((double)t)/(CLOCKS_PER_SEC/1000);
         
         //back propagation
         t = clock();
         backPropogate(selectedNode, board, status);
         t = clock() - t;
         backPropogateTime += ((double)t)/(CLOCKS_PER_SEC/1000);
      }
      //get max child node
      Node* maxChild = getMaxChildNode(root, emptyPositions);
      playNode(board, maxChild);
      destroyNode(root, emptyPositions);
      switchPlayer(board);
   //}
   tWorld = clock() - tWorld;
   printf("Total runtime: %f s\n", ((double)tWorld)/(CLOCKS_PER_SEC));
   puts("Time spent on:");
   printf("  selection: %f ms\n", selectionTime);
   printf("  expansion: %f s\n", expandTime);
   printf("  simulation: %f ms\n", simulateTime);
   printf("  backProp: %f ms\n", backPropogateTime);

   printBoard(*board);
   return 0;
}

/* A bot that finds the first empty space and places its stone */
void lessSmartPlayer(Board* board) {
   if (boardStatus(*board) != IN_PROGRESS) {
      return;
   }
   for (int i = 0; i < board->size; i++) {
      for (int j = 0; j < board->size; j++) {
         if (board->board_game[board->size * i + j] == 0) {
            board->board_game[board-> size * i + j] = board->player;
            return;
         }
      }
   }
}


void printBoard(Board board) {
   for (int i = 0; i < board.size; i++) {
      for (int j = 0; j < board.size; j++) {
         printf("%d ", board.board_game[board.size * i + j]);
      }puts("");
   }
   printf("status = %d\n", boardStatus(board));
   return;
}

void destroyNode(Node* root, int numberOfChildren) {
   for (int i = 0; i < numberOfChildren; i++) {
      free(root->children[i]);
   }
   free(root->children);
   free(root);
   return;
}

void playNode(Board* board, Node* node) {
   board->board_game[board->size * node->positionX + node->positionY] = board->player;
   return;
}

Node* getMaxChildNode(Node* parent, int numberOfChild) {
   Node* tempNode = NULL;
   int tempScore = 0;
   for (int i = 0; i < numberOfChild; i++) {
      if (tempScore <= parent->children[i]->winScore) {
         tempNode = parent->children[i];
         tempScore = tempNode->winScore;
      }
   }
   return tempNode;
}

Node* selection(Node* parentNode, int emptySpace) {
   Node* node = parentNode;
   int parentVisit = parentNode->visits;
   double maxUCB = 0.0;
   for (int i = 0; i < emptySpace; i++) {
      Node* child = parentNode->children[i];
      double nodeUCB;
      // if node has never been visited, give priority to be explored
      if (parentNode->children[i]->visits == 0) { nodeUCB = INT_MAX; } 
      // calculate UCB based on visits and win score
      else { 
         nodeUCB = (parentNode->children[i]->winScore / parentNode->children[i]->visits) + 1.41 * sqrt(log(parentVisit / parentNode->children[i]->visits)); }
      // if the child node has the biggest UCB value, will be explored
      if (nodeUCB >= maxUCB) { node = child; }
   }
   return node;
}

void expandNode(Node* parentNode, Board board, int numberOfEmptyPositions) {
   int* emptyPositions;
   
   // allocate memory for the number of child that will be produced
   parentNode->children = (Node**) malloc(sizeof(Node*) * numberOfEmptyPositions);
   emptyPositions = getEmptyPositions(board, numberOfEmptyPositions);

   for (int i = 0; i < numberOfEmptyPositions; i++) {
      // assign to variable for repeated indexing
      parentNode->children[i] = (Node*) malloc(sizeof(Node));
      parentNode->children[i]->positionX = emptyPositions[i * 2];
      parentNode->children[i]->positionY = emptyPositions[i * 2 + 1];
      parentNode->children[i]->parent = parentNode;
      parentNode->children[i]->children = NULL;
      parentNode->children[i]->player = board.player;
      parentNode->children[i]->visits = 0;
      parentNode->children[i]->winScore = 0;
   }
   free(emptyPositions);
   return;
}

Status simulate(Node* exploringNode, Board* board) {
   Board* tempBoard = initBoard(board->size);
   // position suggested by the node
   int posX = exploringNode->positionX;
   int posY = exploringNode->positionY;
   // make a copy of the board to be stored
   // temp board will not live after this function terminates
   tempBoard->size = board->size;
   tempBoard->player = exploringNode->player;
   // copy the data inside board.board_game to the tempboard.board_game
   for (int i = 0; i < board->size * board->size; i++) {
      tempBoard->board_game[i] = board->board_game[i];
   }
   // place stone chosen by the exploringNode->child node expanded from expandNode()
   tempBoard->board_game[tempBoard->size * posX + posY] =  3 - tempBoard->player;
   Status status = boardStatus(*tempBoard);
   //check imediate spot for losing
   if (status == 3 - board->player) {
      exploringNode->winScore = INT_MAX;
      return status;
   }
   tempBoard->board_game[tempBoard->size * posX + posY] =  tempBoard->player;
   // seed for random play
   srand(time(0));
   // simulate the game until its done
   while(status == IN_PROGRESS) {
      // get all possible movements
      int numberOfPos = numberOfEmptyPositions(*tempBoard);
      int* positions = getEmptyPositions(*tempBoard, numberOfPos);
      // random play
      int randomPlay = rand() % numberOfPos;
      // for correct indexing
      if (randomPlay % 2 == 1) {
         randomPlay +=1;
      }
      if (randomPlay < 0) {
         randomPlay = randomPlay * -1;
      }
      int randomX = positions[randomPlay];
      int randomY = positions[randomPlay + 1];
      switchPlayer(tempBoard);
      tempBoard->board_game[tempBoard->size * randomX + randomY] = tempBoard->player;
      //update status
      status = boardStatus(*tempBoard);
      free(positions);
   }
   free(tempBoard->board_game);
   free(tempBoard);
   return status;
}

void backPropogate(Node* exploredNode, Board* board, Status simulationResult) { 
   Node* tempNode = exploredNode;
   while (tempNode->parent != NULL) {
      tempNode->visits += 1;
      if (tempNode->player == simulationResult) {
         tempNode->winScore += WIN_SCORE;
      }
      tempNode = tempNode->parent;
   }
}
