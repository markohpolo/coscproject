#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <time.h>
#include <stdbool.h>
#include <limits.h>
#include <math.h>
#include <mpi.h>
#include "game.h"
#include "node.h"

#define WIN_SCORE 10

void lessSmartPlayer(Board* board);
void printBoard(Board board);
void destroyNode(Node* root, int numberOfChildren);
void playNode(Board* board, Node* node);
Node* getMaxChildNode(Node* parent, int numberOfChild); 
Node* selection(Node* parentNode, int emptySpace);
Node* convertNode(int* space, int allocationPerProcess);
Status simulate(Node* exploringNode, Board* board);
void backPropogate(Node* exploredNode, Board* board, Status simulationResult);
int* allocateResult(Node* maxChild, int boardSize);

int main(int argc, char** argv) {
   //Initialize the board
   Board* board = initBoard(atoi(argv[1]));
   int numberOfEmptySpace;
   // number of empty spaces allocated per process
   int allocationPerProcess;
   // all empty spaces retrieved by process 0
   int* emptySpaces;
   // empty spaces to be analyzed by each processes
   int* space;
   // node to be simulated
   Node* rootNode; 
   Node* maxChild;
   //timer
   double end, start;
   // max result
   int* maxResult;
   int* allMaxResults;

   // rank for the world
   int my_rank;
   int world_size;
 
   //initialize parallelization
   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &world_size);
   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
   //lessSmartPlayer plays only for root process
   if (my_rank == 0) {
      //lessSmartPlayer(board);
      //switchPlayer(board);
      //this is the expanding stage
      numberOfEmptySpace = numberOfEmptyPositions(*board);
      emptySpaces = getEmptyPositions(*board, numberOfEmptySpace, world_size);
      //prepare memory to gather all results
      allMaxResults = (int*) malloc(world_size * 2 * sizeof(int));
   }

   //broadcast the number of empty space
   MPI_Bcast(&numberOfEmptySpace, 1, MPI_INT, 0, MPI_COMM_WORLD);
   MPI_Bcast(board->board_game, board->size * board->size, MPI_CHAR, 0, MPI_COMM_WORLD);
   MPI_Barrier(MPI_COMM_WORLD);
   start = MPI_Wtime();
   //number of empty spaces allocated per process
   if (numberOfEmptySpace < world_size) { allocationPerProcess = 1; }
   else if (numberOfEmptySpace % world_size > 0) {allocationPerProcess = (numberOfEmptySpace / world_size) + 1;}
   else { allocationPerProcess = numberOfEmptySpace / world_size; }
   
   //allocate memory for available space
   space = (int*) calloc(allocationPerProcess * 2, sizeof(int));
   
   //scatter empty spaces to individual processes
   //MPI_Barrier(MPI_COMM_WORLD);
   MPI_Scatter(emptySpaces, allocationPerProcess * 2, MPI_INT, space, allocationPerProcess * 2, MPI_INT, 0, MPI_COMM_WORLD);
   //convert to node to use existing functions to simualte the game
   MPI_Barrier(MPI_COMM_WORLD);
   // run tree search only for processes < number of empty space
   rootNode = NULL;
   maxChild = NULL;
   MPI_Barrier(MPI_COMM_WORLD);
   if (my_rank < numberOfEmptySpace) {
      rootNode = convertNode(space, allocationPerProcess);
      rootNode->parent = NULL;
      for (int i = 0; i < 100000 / world_size; i++) {
         Node* selectedNode = selection(rootNode, allocationPerProcess);
         Status simulationResult = simulate(selectedNode, board);
         backPropogate(selectedNode, board, simulationResult); 
      }
      maxChild = getMaxChildNode(rootNode, allocationPerProcess);
   }
   maxResult = allocateResult(maxChild, board->size);
   //Gather all results
   MPI_Gather(maxResult, 2, MPI_INT, allMaxResults, 2, MPI_INT, 0, MPI_COMM_WORLD);
   MPI_Barrier(MPI_COMM_WORLD);
   end = MPI_Wtime();
   if (my_rank == 0) {
      int maxPos, maxWinScore = 0;
      for (int i = 0; i < world_size; i++) {
         if (allMaxResults[i * 2 + 1] > maxWinScore) { 
            maxWinScore = allMaxResults[i * 2 + 1];
            maxPos = allMaxResults[i * 2];
         }
      }
      board->board_game[maxPos] = board->player;
      switchPlayer(board);
      printf("time taken for one move: %f\n", end-start);
   }
   MPI_Finalize();
   
}

/* extract the result information to be gathered to process 0 */
int* allocateResult(Node* maxChild, int boardSize) {
   int* result = calloc(2, sizeof(int));
   if (maxChild != NULL) {
      result[0] = maxChild->positionX * boardSize + maxChild->positionY;
      result[1] = maxChild->winScore;
   }
   return result;
}
      

/* A bot that finds the first empty space and places its stone */
void lessSmartPlayer(Board* board) {
   if (boardStatus(*board) != IN_PROGRESS) {
      return;
   }
   for (int i = 0; i < board->size; i++) {
      for (int j = 0; j < board->size; j++) {
         if (board->board_game[board->size * i + j] == 0) {
            board->board_game[board-> size * i + j] = board->player;
            return;
         }
      }
   }
}


void printBoard(Board board) {
   for (int i = 0; i < board.size; i++) {
      for (int j = 0; j < board.size; j++) {
         printf("%d ", board.board_game[board.size * i + j]);
      }puts("");
   }
   printf("status = %d\n", boardStatus(board));
   return;
}

void destroyNode(Node* root, int numberOfChildren) {
   for (int i = 0; i < numberOfChildren; i++) {
      free(root->children[i]);
   }
   free(root->children);
   free(root);
   return;
}

void playNode(Board* board, Node* node) {
   board->board_game[board->size * node->positionX + node->positionY] = board->player;
   return;
}

Node* getMaxChildNode(Node* parent, int numberOfChild) {
   Node* tempNode = NULL;
   int tempScore = 0;
   if (parent == NULL) { return tempNode; }
   for (int i = 0; i < numberOfChild; i++) {
      if (parent->children[i] == NULL) { continue; }
      if (tempScore <= parent->children[i]->winScore) {
         tempNode = parent->children[i];
         tempScore = tempNode->winScore;
      }
   }
   return tempNode;
}

Node* selection(Node* parentNode, int emptySpace) {
   Node* node = parentNode;
   int parentVisit = parentNode->visits;
   double maxUCB = 0.0;
   for (int i = 0; i < emptySpace; i++) {
      double nodeUCB;
      if (parentNode->children[i] == NULL) { continue; }
      // if node has never been visited, give priority to be explored
      if (parentNode->children[i]->visits == 0) { nodeUCB = INT_MAX; } 
      // calculate UCB based on visits and win score
      else { 
         nodeUCB = (parentNode->children[i]->winScore / parentNode->children[i]->visits) + 1.41 * sqrt(log(parentVisit / parentNode->children[i]->visits)); 
      }
      // if the child node has the biggest UCB value, will be explored
      if (nodeUCB >= maxUCB) { node = parentNode->children[i]; }
   }
   return node;
}


Node* convertNode(int* space, int allocationPerProcess) {
   int posX, posY;
   Node* parentNode = malloc(sizeof(Node));
   parentNode->children = malloc(sizeof(Node*) * allocationPerProcess);
   for (int i = 0; i < allocationPerProcess; i++) {
      posX = space[i * 2];
      posY = space[i * 2 + 1]; 
      if (posX == 0 && posY == 0) {
         parentNode->children[i] = NULL;
         continue;
      }
      // assign to variable for repeated indexing
      parentNode->children[i] = (Node*) malloc(sizeof(Node));
      parentNode->children[i]->positionX = posX;
      parentNode->children[i]->positionY = posY;
      parentNode->children[i]->parent = parentNode;
      parentNode->children[i]->children = NULL;
      parentNode->children[i]->player = PLAYER_X;
      parentNode->children[i]->visits = 0;
      parentNode->children[i]->winScore = 0;
   }
   return parentNode;
}

Status simulate(Node* exploringNode, Board* board) {
   Board* tempBoard = initBoard(board->size);
   // position suggested by the node
   int posX = exploringNode->positionX;
   int posY = exploringNode->positionY;
   // make a copy of the board to be stored
   // temp board will not live after this function terminates
   tempBoard->size = board->size;
   tempBoard->player = exploringNode->player;
   // copy the data inside board.board_game to the tempboard.board_game
   for (int i = 0; i < board->size * board->size; i++) {
      tempBoard->board_game[i] = board->board_game[i];
   }
   // place stone chosen by the exploringNode->child node expanded from expandNode()
   tempBoard->board_game[tempBoard->size * posX + posY] =  3 - tempBoard->player;
   Status status = boardStatus(*tempBoard);
   //check imediate spot for losing
   if (status == 3 - board->player) {
      exploringNode->winScore = INT_MAX;
      return status;
   }
   tempBoard->board_game[tempBoard->size * posX + posY] =  tempBoard->player;
   // seed for random play
   srand(time(0));
   // simulate the game until its done
   while(status == IN_PROGRESS) {
      // get all possible movements
      int numberOfPos = numberOfEmptyPositions(*tempBoard);
      int* positions = getEmptyPositions(*tempBoard, numberOfPos, 1);
      // random play
      int randomPlay = rand() % numberOfPos;
      // for correct indexing
      if (randomPlay % 2 == 1) {
         randomPlay +=1;
      }
      if (randomPlay < 0) {
         randomPlay = randomPlay * -1;
      }
      int randomX = positions[randomPlay];
      int randomY = positions[randomPlay + 1];
      switchPlayer(tempBoard);
      tempBoard->board_game[tempBoard->size * randomX + randomY] = tempBoard->player;
      //update status
      status = boardStatus(*tempBoard);
      free(positions);
   }
   free(tempBoard->board_game);
   free(tempBoard);
   return status;
}

void backPropogate(Node* exploredNode, Board* board, Status simulationResult) { 
   exploredNode->visits += 1;
   if (exploredNode->player == simulationResult) {
      exploredNode->winScore += WIN_SCORE;
   }
}
