#ifndef NODE_H
#define NODE_H

#include <stdio.h>
#include <stdlib.h>
#include "game.h"

// Single node of the tree structure
typedef struct Nodes
{
   struct Nodes* parent;
   struct Nodes** children;
   int positionX;
   int positionY;
   int visits;
   double winScore;
   char player;
} Node;

#endif
