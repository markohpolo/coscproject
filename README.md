# **Description**
This project investigates different techniques of parallelizing the Monte-Carlo Tree search by utilizing a reziable tic-tac-toe game.

## **Approach**
1. Serial implementation of a resizabe tic-tac-toe ([no_optimization](https://gitlab.com/markohpolo/coscproject/-/tree/master/Project/no_optimization)), bottleneck of the implementation has been investigated using gprof ([analysis](https://gitlab.com/markohpolo/coscproject/-/blob/master/Project/no_optimization/analysis.txt)).
2. Minimal optimzation of the serial code (avoiding cache misses and compiling with -O3 flag)
3. Optimization using openMP ([openmp](https://gitlab.com/markohpolo/coscproject/-/tree/master/Project/openmp))
4. Optimization using MPI ([MPI](https://gitlab.com/markohpolo/coscproject/-/tree/master/Project/parallel))

## **Usage & Output**
- All approaches described from the section above can be compiled with `make` command.
`make` produces a single executable file named `mcts`. Makefiles are located in each directories.

### **Common Output**
All executables (`mcts`) have different usage, however has a common output format.\
**Example - serial implementation**
```
bash$./mcts 5
Total runtime: 24.470000 s
Time spent on:
    selection: 2410.000000 ms
    simulation: 14330.000000 ms
    backProp: 2040.000000 ms
2 1 1 1 2
1 2 2 2 1
1 1 1 2 1
1 2 1 2 2
1 2 1 2 2
status = 3
```
**Status = 1** player 1 wins\
**Status = 2** player 2 wins\
**Status = 3** draw\

**gprof**\
Runtime analysis of the serial implementation can be done by using gprof profiler.\
Executing the serial implementations will output a `gmon.out` file. In order to produce the readable profile, the following commands should be executed in the command line.
```
gprof mcts gmon.out > analysis.txt
```

**Serial code execution**\
```
./mcts n
```
where `n` is the size of the n x n board\
**openMP code execution** 
```
export OMP_NUM_THREADS = N
./mcts n
```
where `N` is the number of threads to run\
**MPI code execution**
```
mpiexec -N 4 ./mcts n
```
where `N` is the number of MPI nodes to run\

**htop**\
`htop`  can be used during the code execution of MPI and openMP executables to view the resoure usage

## **Results and implementation detail**
Results and implementation detail can be found ([here](https://gitlab.com/markohpolo/coscproject/-/wikis/Results-and-details))

## **Blockers & To do**
- MPI approach with more than 10 nodes fails
- Hybrid(MPI + openMP) implementation of the game play
- Cuda apporach for optimization
